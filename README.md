# paper-shift

this Project is a code challenge that have been developed for Papershif company.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

**FrontEnd Libraries and frameworks**:

  

1 - VueJS - VUEX - Vue Router

2 - CSS Modules

3 - SASS (there isn't so much Sass and CSS because no need to)

4 - CYPRESS for END2END Test.

5 - eslint and prettier for lint and format files

6 - unit test with Vue test utils and jest 

7- Typescript for Typecheckings

8- docker and docker compose to run easily

  

# Run project

  

if you prefer Docker you can use docker files or docker compose. the more detailed information is in [docker](https://gitlab.com/mhos.malek/papershift-vue-js-app/-/tree/master/doc/docker) docs..

  

otherwise, you can go to projects folder and run the project with scripts provided above.

if you build the frontend project you can build it with npm run build. all the files will be going to save at dist folder.

you can serve it with npm package called "serve"

for install server:

    yarn add global serve

then run local server with:

     serve dist

# Run project

project has been deployed to Surge. there is alos a gitlab CI and project can deploy to surge after create new Tag in GitlabCI.


# Project Structure



the main purpose of this project is about moving the main logic from UI to VUEX.

```

.


├── dist # Compiled files (alternative of `build` in other frameworks)

├── src # Source files

│ ├── assets # main assets directory

│ │ ├── img # images realted assets

│ ├── components # this folder including everything that we shows in ui(base components, layouts, pages, screens and ...)

│ │ ├── base # common and mostly stateless components like button, card, grid and lazy load 

│ │ ├── cart # cart module components that will used on cart page or any component that need cart data

│ │ ├── product # product module components that will used on cart page or any component that need product data

│ ├── direcives # main directives folder

│ ├── errorhandler # the errorhandler so every error can be handled from one place

│ ├── filter # the main filters in one place

│ ├── layouts # layout component that include header, actionbar(logout for now), navbar, footer and ...

│ ├── mixins # mixins that will be used for authenctiction. I also created dummy mixin for roles. 

│ ├── mock backend # data provider and api mocks to handle data part in app.

│ ├── store # main store folder that includes modules in app with main store files like mutations and ...

│ │ ├── cart # the Cart module with type included and handle shopping Cart.

│ │ ├── auth # the Auth module with type included and handle Authentication Related data.

│ │ ├── product # the Product module with type included and handle products page related data.

│ ├── styles # styles and modules that provide atomic classess for other components.

│ ├── services # services is based on some utils that provide specefic feature in any component.

│ ├── utils # main utils of project. anything that do onething and has no relation to the project itself and can be used anywhere else.

│ ├── Views # main views and pages of project. any thing that will be used on router only.

│ ├── App.ts # main app file

│ ├── custom-properties.d.ts # main file to declare custom modules for typescript (not used here)


├── test # Automated tests (Cypress tests for end2end and functional and (jest and vue test utils) for unit tests)

├── Dockerfile # docker file to run in development server on port 8080 

├── Dockerfile.prod # docker file to build in production wepack config and serve with nginx

├── docker-compose # docker-compose file to build and run production docker file

├── doc # Documentation files (alternatively `doc`)

├── shims-vue.d.ts #declare global vue types

│ ├── docker.md # Table of contents

├── LICENSE

└── README.md

└── ... # etc.

  

```

  

## Components:

  

there is a component that includes this types:

- Base-components: components like loading, icon and ... that they are stateless and never connect ro VUEX or any thing else. they just get props and show the result!

- VIEWS: this components that render pages in router

- layout components: main layout related like header, footer.

- module-related components: the components that would be shared in pages or layouts and they can be connected to VUEX.like product or cart components
  
  
  
  

## VUEX Structure

  
the project has 3 main modules with their own getters, mutations and actions and typecheckings.
  
  

## Styles:

  I haven't done anything special in here. there is CSS Modules and some custom styles for some components.

  

## mixins

  

the mixins are main handlers of logics in the application. they provide context for every feature of the app. the most important providers in there.

  

## services

services is some functions that provide specefic feature and usualy.

  

## FrontEnd TEST - END2END


I used cypress as my main test framework and icnludes some tests in here.

  
  