module.exports = {
  runtimeCompiler: true,
  css: {
    loaderOptions: {
      scss: {
        prependData: `
                        @import "@/styles/_variables.scss";
                        @import "@/styles/index.scss";
                    `
      }
    }
  },
  chainWebpack: config => {
    // add custom font support
    config.module
      .rule("fonts")
      .test(/\.(ttf|otf|eot|woff|woff2)$/)
      .use("url-loader")
      .loader("url-loader")
      .end();
  }
};
