import { createMockLocalVue } from "../utils";

describe("Auth local Storage section", () => {
  let localVue;
  beforeAll(() => {
    localVue = createMockLocalVue();
  });

  it("should be authenticated on set token in local storage", async () => {
    window.localStorage.setItem("dummyAccessToken", "accessToken");
    expect(window.localStorage.getItem("dummyAccessToken")).toBe("accessToken");
  });
  it("should be not authenticated on remove token form local storage", async () => {
    window.localStorage.removeItem("dummyAccessToken");
    expect(window.localStorage.getItem("dummyAccessToken")).toBe(null);
  });
});
