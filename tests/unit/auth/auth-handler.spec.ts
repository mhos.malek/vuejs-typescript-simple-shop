import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import Vuex from "vuex";
import HomePage from "@/views/Home.vue";
import CartPage from "@/views/Cart.vue";
import AddProductListItem from "../../../src/components/cart/AddedProductsListItem/index.vue";
import RelatedProductListItem from "../../../src/components/cart/RelatedProductsListItem/index.vue";

import CartMockDataResponse from "@/mock-backend/data/cart.json";
import delay from "@/utils/genral";
import MutationTypes from "@/store/modules/auth/mutation-types";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);
const router = new VueRouter();
const $route = {
  path: "/cart"
};

describe("Auth Handler", () => {
  let actions: any;
  let store: any;

  beforeEach(() => {
    jest.spyOn(console, "error").mockImplementation(() => {});
    //   mock store
    actions = {
      getCartDetails: jest.fn(),
      getProducts: jest.fn()
    };
    const mutations = {
      increaseItemInCard: jest.fn(),
      decreaseItemInCard: jest.fn()
    };
    const getters = {
      totalQuantityGetter: () => 4,
      totalPriceGetter: () => 190,
      similarProductsGetter: () => CartMockDataResponse.simliarProducts,
      productsInCart: () => CartMockDataResponse.addedProductsToCart
    };
    store = new Vuex.Store({
      actions,
      getters,
      mutations
    });
  });

  it('it should dispatch "REQUESTS_USER_LOGIN_FULFILLED" and user shoud be authenticated', () => {
    const wrapper = shallowMount(HomePage, {
      store,
      localVue,
      mocks: {
        $route
      }
    });
    wrapper.vm.$store.commit(MutationTypes.REQUESTS_USER_LOGIN_FULFILLED);
    delay(1000).then(() =>
      expect(wrapper.vm.$store.state.isAuthenticated).toBeTruthy()
    );
  });
  it('it should dispatch "REQUESTS_USER_LOGOUT_FULFILLED" and user shoud be logout', () => {
    const wrapper = shallowMount(HomePage, {
      store,
      localVue,
      mocks: {
        $route
      }
    });
    wrapper.vm.$store.commit(MutationTypes.REQUESTS_USER_LOGOUT_FULFILLED);
    delay(1000).then(() =>
      expect(wrapper.vm.$store.state.isAuthenticated).toBeFalsy()
    );
  });
  it("it should not be redirect  when it is logged in", () => {
    const wrapper = shallowMount(HomePage, {
      store,
      localVue,
      mocks: {
        $route
      }
    });
    wrapper.vm.$store.commit(MutationTypes.REQUESTS_USER_LOGIN_FULFILLED);
    delay(1000).then(() => expect(wrapper.vm.$route.path).toBe("/cart"));
  });

  it("it should  be redirect to home page when user is not logged in", () => {
    const wrapper = shallowMount(HomePage, {
      store,
      localVue,
      mocks: {
        $route
      }
    });
    wrapper.vm.$store.commit(MutationTypes.REQUESTS_USER_LOGOUT_FULFILLED);
    delay(1500).then(() => expect(wrapper.vm.$route.path).toBe("/"));
  });
});
