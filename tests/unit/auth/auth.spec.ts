import { mount } from "@vue/test-utils";

import HomePage from "@/views/Home.vue";
import { createMockLocalVue, createAuthMockMixin } from "../utils";
import delay from "@/utils/genral";

describe("Auth routes test", () => {
  let localVue: any;
  let wrapper: any;
  let LoginButton: any;
  let LogoutButton: any;

  //   mock calls
  const handleClick = jest.fn();
  beforeAll(() => {
    localVue = createMockLocalVue();
  });
  beforeEach(() => {
    wrapper = mount(HomePage, {
      localVue,
      mixins: [createAuthMockMixin()]
    });
    const buttonsList = wrapper.findAll("button");
    LoginButton = buttonsList.filter((item: any) => item.text() === "Login");
    LogoutButton = buttonsList.filter((item: any) => item.text() === "Logout");
  });

  it("should not be authenticated on mount", async () => {
    const authElem: any = wrapper.find("p:nth-child(3)");
    expect(authElem.text()).toContain("You Are not Authenticated!");
  });

  it("should be authenticated on click on Button", async () => {
    await window.localStorage.setItem("dummyAccesstoken", "asd");
    const authElem: any = wrapper.find("p:nth-child(3)");
    delay(1000).then(() => {
      expect(authElem.text()).toContain("You Are Authenticated!");
    });
  });

  it("should be authenticated after click on login button", async () => {
    await LoginButton.trigger("click");
    delay(1000).then(() => {
      expect(handleClick).toHaveBeenCalledTimes(1);
      const authElem: any = wrapper.find("p:nth-child(3)");
      expect(authElem.text()).toContain("You Are Authenticated!");
    });
  });

  it("should be authenticated after click on login button", async () => {
    await LogoutButton.trigger("click");
    delay(1000).then(() => {
      expect(handleClick).toHaveBeenCalledTimes(1);
      const authElem: any = wrapper.find("p:nth-child(3)");
      expect(authElem.text()).toContain("You Are not Authenticated!");
    });
  });
});
