import Vuex from "vuex";
import Vue from "vue";
import VueRouter from "vue-router";
import HomePage from "@/views/Home.vue";
import { createLocalVue } from "@vue/test-utils";

enum AUTH_CONTENT {
  LOGIN = "Hey! you Are Authenticated",
  LOGOUT = "You Are not Authenticated!"
}

const createAuthMockMixin = () => {
  const authMixin = Vue.extend({
    created: function() {
      this.checkUserAuthentication();
      if (!this.isAuthenticatedFromAuthMixin) {
        // handle user is not authenticatd
        // for this code challeng I havent authenticated user so we dont have login page and I redirect him/her to main page
      }
      const hasToken = true;
      if (hasToken) {
        // login user in store
        this.REQUESTS_USER_LOGIN_FULFILLED();
      } else {
        // logout user in store
        this.REQUESTS_USER_LOGOUT_FULFILLED();
      }
    },
    computed: {
      isAuthenticatedFromAuthMixin: () =>
        window.localStorage.getItem("dummyAccessToken"),
      isAuthenticationPending: () => false
    },
    methods: {
      loginUserFromAuthMixin: () => null,
      logOutUserFromAuthMixin: () => null,
      REQUESTS_USER_LOGIN_FULFILLED: () => null,
      REQUESTS_USER_LOGOUT_FULFILLED: () => null,

      checkUserAuthentication: function() {
        if (
          !this.isAuthenticationPending &&
          this.isAuthenticatedFromAuthMixin
        ) {
          return true;
        }
        return false;
      }
    }
  });
  return authMixin;
};

const defaultRoutes = [{ path: "/", component: HomePage }];

const createMockRoutes = (routes = defaultRoutes) => {
  const router = new VueRouter({
    routes
  });
};
const createMockLocalVue = () => {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  localVue.use(Vuex);
  return localVue;
};

export {
  createAuthMockMixin,
  AUTH_CONTENT,
  createMockLocalVue,
  createMockRoutes
};
