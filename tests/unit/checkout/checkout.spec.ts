import { shallowMount, createLocalVue, mount } from "@vue/test-utils";
import Vuex from "vuex";
import CartPage from "@/views/Cart.vue";
import AddProductListItem from "../../../src/components/cart/AddedProductsListItem/index.vue";
import RelatedProductListItem from "../../../src/components/cart/RelatedProductsListItem/index.vue";

import CartMockDataResponse from "@/mock-backend/data/cart.json";
import delay from "@/utils/genral";
const sampleProductData = {
  title: "sample Proudct",
  price: 19,
  image: "main.jpeg",
  id: 1,
  quantity: 1
};
const localVue = createLocalVue();

localVue.use(Vuex);

describe("Checkout flow unit test", () => {
  let actions: any;
  let store: any;

  beforeEach(() => {
    jest.spyOn(console, "error").mockImplementation(() => {});
    //   mock store
    actions = {
      getCartDetails: jest.fn(),
      getProducts: jest.fn()
    };
    const mutations = {
      increaseItemInCard: jest.fn(),
      decreaseItemInCard: jest.fn()
    };
    const getters = {
      totalQuantityGetter: () => 4,
      totalPriceGetter: () => 190,
      similarProductsGetter: () => CartMockDataResponse.simliarProducts,
      productsInCart: () => CartMockDataResponse.addedProductsToCart
    };
    store = new Vuex.Store({
      actions,
      getters,
      mutations
    });
  });

  it('it should dispatch "getCartDetails" action when user enters the page', () => {
    const wrapper = shallowMount(CartPage, { store, localVue });
    const spy = jest.spyOn(actions, "getCartDetails");
    // wait to make sure event is dispatched
    delay(1000).then(() => expect(actions.getCartDetails).toHaveBeenCalled());
  });

  it('it should show the correct getter result for "SimilarProductsGetter"', () => {
    const wrapper = shallowMount(CartPage, { store, localVue });
    const result = wrapper.vm.$store.getters.similarProductsGetter;
    expect(result).toMatchObject(CartMockDataResponse.simliarProducts);
  });

  it('it should show the correct getter result for "totalPriceGetter"', () => {
    const wrapper = shallowMount(CartPage, { store, localVue });
    const result = wrapper.vm.$store.getters.totalPriceGetter;
    expect(result).toBe(190);
  });

  it('it should show the correct getter result for "totalQuantityGetter"', () => {
    const wrapper = shallowMount(CartPage, { store, localVue });
    const result = wrapper.vm.$store.getters.totalQuantityGetter;
    expect(result).toBe(4);
  });
  it('it should increase the  getter result for "totalQuantityGetter" after click on add product', async () => {
    const wrapper = mount(AddProductListItem, {
      store,
      localVue,
      propsData: sampleProductData
    });
    const buttons = wrapper.findAll("button");
    const plusButton = buttons
      .filter(
        item =>
          item.element.children.item(0)?.classList.value ===
          "icon icon-plus-circle"
      )
      .at(0);
    const minusButton = buttons
      .filter(
        item =>
          item.element.children.item(0)?.classList.value ===
          "icon icon-minus-circle"
      )
      .at(0);
    await plusButton.trigger("click");
    delay(1000).then(() =>
      expect(wrapper.vm.$store.getters.totalQuantityGetter).toBe(5)
    );
  });
  it('it should decrease the  getter result for "totalQuantityGetter" after click on mius icon on the  product', async () => {
    const wrapper = mount(AddProductListItem, {
      store,
      localVue,
      propsData: sampleProductData
    });
    const buttons = wrapper.findAll("button");

    const minusButton = buttons
      .filter(
        item =>
          item.element.children.item(0)?.classList.value ===
          "icon icon-minus-circle"
      )
      .at(0);
    await minusButton.trigger("click");
    delay(1000).then(() =>
      expect(wrapper.vm.$store.getters.totalQuantityGetter).toBe(3)
    );
  });

  it("it should add the product to cart after click on add to cart button on related product", async () => {
    const wrapper = mount(RelatedProductListItem, {
      store,
      localVue,
      propsData: sampleProductData
    });
    const addToCartbutton = wrapper.find("button");
    await addToCartbutton.trigger("click");

    delay(1000).then(() =>
      expect(wrapper.vm.$store.getters.totalPriceGetter).toBe(209)
    );
  });
  it("it should add the product to cart after click on add to cart button on related product", async () => {
    const wrapper = mount(RelatedProductListItem, {
      store,
      localVue,
      propsData: sampleProductData
    });
    const addToCartbutton = wrapper.find("button");
    await addToCartbutton.trigger("click");

    delay(1000).then(() =>
      expect(wrapper.vm.$store.getters.productsInCart).not.toMatch(
        // @ts-ignore
        CartMockDataResponse.addedProductsToCart
      )
    );
  });
});
