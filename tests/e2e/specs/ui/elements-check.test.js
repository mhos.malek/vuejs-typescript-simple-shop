/// <reference types="cypress" />

context("test UI elements and their visibilry", () => {
  beforeEach(() => {
    cy.visit(Cypress.env("CLIENT_APPLICATION_URL"));
    cy.wait(3000);
  });

  it("should have container class", () => {
    cy.xpath("/html/body/div/div/div/div/div").should(
      "have.class",
      "container_baseContainer_32eZw"
    );
  });

  it("should have header cart class", () => {
    cy.xpath("/html/body/div/div/div/div/div/div[2]/div/section/header")
      .children()
      .should("have.class", "Cart_header_3Go6y");
  });
});
