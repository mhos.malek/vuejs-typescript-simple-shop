import { DirectiveBinding } from "vue/types/options";

export default {
  inserted: (el: HTMLElement, binding: DirectiveBinding) => {
    const mediaQueryString = `(${binding.arg ? binding.arg : "max-width"}: ${
      binding.value
    })`;
    const mediaQuery = window.matchMedia(mediaQueryString);
    if (mediaQuery.matches) {
      el.style.display = "none";
    }
  }
};
