export default {
  inserted: (el: Element) => {
    function loadImage() {
      const children = Array.from(el.children);
      const imageElement: HTMLImageElement | any = children.find(
        (el: Element) => el.nodeName === "IMG"
      );
      const placeHolderElement: HTMLImageElement | any = children.find(
        (el: Element) => {
          return el.attributes.getNamedItem("data-placeholder");
        }
      );

      if (imageElement) {
        imageElement.addEventListener("load", () => {
          setTimeout(() => {
            el.classList.add("loaded");
          }, 100);
        });
        imageElement.addEventListener("error", () => console.log("error"));
        placeHolderElement.style.display = "none";
        imageElement.src = imageElement.dataset.url;
      }
    }

    function handleIntersect(
      entries: Array<IntersectionObserverEntry>,
      observer: IntersectionObserver
    ) {
      entries.forEach((entry: IntersectionObserverEntry) => {
        if (entry.isIntersecting) {
          loadImage();
          observer.unobserve(el);
        }
      });
    }

    interface IntersectionObserverOptions {
      root: null | Element;
      threshold: number;
    }

    function createObserver() {
      const options: IntersectionObserverOptions = {
        root: null,
        threshold: 0
      };
      const observer = new IntersectionObserver(handleIntersect, options);
      observer.observe(el);
    }
    if (window["IntersectionObserver"]) {
      createObserver();
    } else {
      loadImage();
    }
  }
};
