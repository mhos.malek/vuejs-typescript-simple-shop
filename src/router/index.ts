import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import authHandler from "./auth-handler";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    meta: {
      isPublic: true
    },
    component: () => import(/* webpackChunkName: "Home" */ "../views/Home.vue")
  },
  {
    path: "/cart",
    name: "Cart",
    meta: {
      isPublic: false
    },
    component: () =>
      import(/* webpackChunkName: "product" */ "../views/Cart.vue")
  },
  {
    path: "/product/:productId",
    name: "ProductDetails",
    meta: {
      isPublic: false
    },
    component: () =>
      import(/* webpackChunkName: "product" */ "../views/ProductDetails.vue")
  },
  {
    path: "*",
    name: "NotFound",
    meta: {
      isPublic: true
    },
    component: () => import("../views/NotFound.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(authHandler);

export default router;
