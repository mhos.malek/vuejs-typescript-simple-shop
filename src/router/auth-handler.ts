export const checkUserAuthenticationStatus = () =>
  window.localStorage.getItem("dummyAccessToken");
const authHandler = (
  to: { meta?: any },
  from: any,
  next: (arg?: any) => void
) => {
  const isAuthenticated = checkUserAuthenticationStatus();
  if (!to.meta.isPublic) {
    if (!isAuthenticated) {
      next({ name: "Home" });
    } else {
      next();
    }
  } else next();
};
export default authHandler;
