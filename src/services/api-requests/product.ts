import MockBackend from "@/mock-backend/server";

const getProductByIdApi = (productId: number): Promise<any> => {
  return MockBackend.getProductById(false, productId);
};

const getCardDetailsApi = (): Promise<any> => {
  return MockBackend.getCartDetails();
};

export { getProductByIdApi, getCardDetailsApi };
