import MockBackend from "@/mock-backend/server";

const loginUserApi = (): Promise<any> => {
  return MockBackend.login();
};

const logOutUserApi = (): Promise<any> => {
  return MockBackend.logout();
};

export { loginUserApi, logOutUserApi };
