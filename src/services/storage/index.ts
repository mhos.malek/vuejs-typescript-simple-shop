class StorageService {
  defaultStorage: WindowLocalStorage;
  constructor() {
    this.defaultStorage = window;
    if (!this.checkLocalAndSessionStorageSupport()) {
      throw new Error(
        "local and session storage is not supported in your device"
      );
    }
  }
  checkLocalAndSessionStorageSupport = (): boolean => {
    if (!window.localStorage || !window.sessionStorage) {
      return false;
    }
    return true;
  };
  setItem = (key: string, value: any): void => {
    return this.defaultStorage.localStorage.setItem(key, value);
  };

  getItem = (key: string, isJson = false): any => {
    return this.defaultStorage.localStorage.getItem(
      isJson ? JSON.parse(key) : key
    );
  };

  removeItem = (key: string): void => {
    return this.defaultStorage.localStorage.removeItem(key);
  };

  clear = (): void => {
    return this.defaultStorage.localStorage.clear();
  };
}

export default new StorageService();
