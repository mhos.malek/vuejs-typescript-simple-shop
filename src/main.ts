import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import capitalize from "./filters/capitalize";
import hideOn from "./directives/responsive";

Vue.config.productionTip = true;

// register filtes and direvice that need globaly
Vue.filter("capitalize", capitalize);
Vue.directive("responsive", hideOn);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
