const delay = (timeInMs: number) => {
  return new Promise(res => setTimeout(() => res(), timeInMs));
};

export default delay;
