import { StateType } from "./types";

const state: StateType = {
  isAuthenticated: false,
  isAuthenticationPending: false
};

export default state;
