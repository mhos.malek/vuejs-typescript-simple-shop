enum ActionTypes {
  LOGIN_USER_ACTION = "LOGIN_USER_ACTION",
  LOGOUT_USER_ACTION = "LOGOUT_USER_ACTION"
}

export default ActionTypes;
