import { GettersType } from "./types";

const getters: GettersType = {
  getAuthenticationStatusGetter: state => state.isAuthenticated,
  getAuthenticationIsPendingGetter: state => state.isAuthenticationPending
};

export default getters;
