import MutationsType from "./mutation-types";
import { MutationTree } from "vuex";
import { StateType, MutationsTypeInterface } from "./types";

const mutations: MutationTree<StateType> & MutationsTypeInterface = {
  [MutationsType.REQUESTS_USER_LOGIN](state: StateType): void {
    state.isAuthenticationPending = true;
  },
  [MutationsType.REQUESTS_USER_LOGIN_FULFILLED](state): void {
    state.isAuthenticated = true;
    state.isAuthenticationPending = false;
  },
  [MutationsType.REQUESTS_USER_LOGIN_FAILD](state): void {
    state.isAuthenticated = false;
    state.isAuthenticationPending = false;
  },

  [MutationsType.REQUESTS_USER_LOGOUT](state): void {
    state.isAuthenticationPending = true;
  },

  [MutationsType.REQUESTS_USER_LOGOUT_FULFILLED](state): void {
    state.isAuthenticated = false;
    state.isAuthenticationPending = false;
  },
  [MutationsType.REQUESTS_USER_LOGOUT_FAILD](state): void {
    state.isAuthenticationPending = false;
  }
};

export default mutations;
