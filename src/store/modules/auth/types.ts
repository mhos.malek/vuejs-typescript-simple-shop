import { Commit } from "vuex";
import MutationTypes from "./mutation-types";
import ActionTypes from "./action-types";

// init State Types:
type StateType = {
  isAuthenticated: boolean;
  isAuthenticationPending: boolean;
};

// MutationTypes
type MutationsTypeInterface<S = StateType> = {
  [MutationTypes.REQUESTS_USER_LOGIN](state: S): void;
  [MutationTypes.REQUESTS_USER_LOGIN](state: S): void;
  [MutationTypes.REQUESTS_USER_LOGIN_FULFILLED](state: S): void;
  [MutationTypes.REQUESTS_USER_LOGIN_FAILD](state: S): void;
  [MutationTypes.REQUESTS_USER_LOGOUT](state: S): void;
  [MutationTypes.REQUESTS_USER_LOGOUT_FULFILLED](state: S): void;
  [MutationTypes.REQUESTS_USER_LOGOUT_FAILD](state: S): void;
};

// default VUEX Types
interface ActionTypeDefault {
  commit: Commit;
}

// action Types
interface ActionsTypeInterface {
  [ActionTypes.LOGIN_USER_ACTION]({ commit }: ActionTypeDefault): Promise<any>;
  [ActionTypes.LOGOUT_USER_ACTION]({ commit }: ActionTypeDefault): Promise<any>;
}

type GettersType = {
  getAuthenticationStatusGetter: (state: StateType) => boolean;
  getAuthenticationIsPendingGetter: (state: StateType) => boolean;
};

export {
  ActionTypeDefault,
  StateType,
  ActionsTypeInterface,
  GettersType,
  MutationsTypeInterface
};
