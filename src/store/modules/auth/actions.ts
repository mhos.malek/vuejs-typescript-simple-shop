import MutationTypes from "./mutation-types";
import { ActionsTypeInterface } from "./types";
import { loginUserApi, logOutUserApi } from "@/services/api-requests/auth";
import ActionTypes from "./action-types";
import { ActionTree } from "vuex";
import { StateType } from "../cart/types";

const actions: ActionTree<StateType, any> & ActionsTypeInterface = {
  async [ActionTypes.LOGIN_USER_ACTION]({ commit }) {
    commit(MutationTypes.REQUESTS_USER_LOGIN);
    try {
      await loginUserApi();
      commit(MutationTypes.REQUESTS_USER_LOGIN_FULFILLED);
    } catch (error) {
      commit(MutationTypes.REQUESTS_USER_LOGOUT_FAILD);
    }
  },
  async [ActionTypes.LOGOUT_USER_ACTION]({ commit }) {
    commit(MutationTypes.REQUESTS_USER_LOGOUT);
    try {
      await logOutUserApi();
      commit(MutationTypes.REQUESTS_USER_LOGOUT_FULFILLED);
    } catch (error) {
      commit(MutationTypes.REQUESTS_USER_LOGOUT_FAILD);
    }
  }
};

export default actions;
