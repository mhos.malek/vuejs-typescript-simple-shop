import MutationTypes from "./mutation-types";
import {
  MutationsTypeInterface,
  ProductDeatilsType,
  StateType,
  ChangeQuantityOfProduct
} from "./types";
import CartData from "@/mock-backend/data/cart.json";

const updateQuantity = (
  state: StateType,
  payload: ChangeQuantityOfProduct,
  selectedItemQuantity: number
) => {
  return state.cart.products.reduce(
    (res: Array<ProductDeatilsType>, current: any) => {
      if (current.id === payload.id) {
        if (payload.type === "increase") {
          current.quantity = current.quantity + 1;
        }
        if (payload.type === "decrease") {
          if (selectedItemQuantity > 0) {
            current.quantity = current.quantity - 1;
          }
        }
      }
      res.push(current);
      return res;
    },
    []
  );
};

const mutations: MutationsTypeInterface = {
  [MutationTypes.REQUESTS_CART_LIST](state) {
    state.requestCardPending = true;
  },

  [MutationTypes.REQUESTS_CART_LIST_FULFILLED](state, payload: any): void {
    state.requestCardPending = false;
    const { addedProductsToCart, simliarProducts, allProducts } = payload;
    state.simliarProducts = simliarProducts;
    state.cart.products = [...addedProductsToCart];
    state.allProducts = allProducts;
    state.totalQuantity = addedProductsToCart.reduce(
      (result: number, current: ProductDeatilsType) => {
        result += current.quantity ? current.quantity : 0;
        return result;
      },
      0
    );
    state.totalPrice = addedProductsToCart.reduce(
      (result: number, current: any) => {
        result += current.price;
        return result;
      },
      0
    );
  },
  [MutationTypes.REQUESTS_CART_LIST_FAILD](state): void {
    state.requestCardPending = false;
  },

  [MutationTypes.ADD_NEW_ITEM_TO_CART](state, payload): void {
    const selectedItem: any = state.allProducts.filter(
      product => product.id === payload.id
    )[0];
    if (!selectedItem) {
      return;
    }
    selectedItem.quantity = 1; // set product quantity to 1 at first
    state.cart.products = [...state.cart.products, selectedItem];
    state.simliarProducts = state.simliarProducts.filter(
      product => product.id !== payload.id
    );
    state.totalQuantity += 1;
    state.totalPrice += selectedItem.price;
  },
  [MutationTypes.REMOVE_ITEM_FROM_CART](state, payload) {
    const selectedItem: any = state.allProducts.filter(
      product => product.id === payload.id
    )[0];
    state.cart.products = state.cart.products.filter(
      product => product.id !== payload.id
    );
    state.totalQuantity -= selectedItem.quantity;
    state.totalPrice -= selectedItem.price;
  },
  [MutationTypes.REMOVE_ALL_ITEMS_IN_CART](state) {
    state.cart.products = [];
    state.simliarProducts = CartData.allProducts;
    state.totalQuantity = 0;
    state.totalPrice = 0;
  },
  [MutationTypes.CHANGE_ITEM_IN_CART](state, payload): void {
    const selectedItem: any = state.cart.products.filter(
      product => product.id === payload.id
    )[0];
    const updatedProductItem: Array<ProductDeatilsType> = updateQuantity(
      state,
      payload,
      selectedItem.quantity
    );
    state.cart.products = updatedProductItem;
    if (payload.type === "increase") {
      state.totalQuantity += 1;
      state.totalPrice += selectedItem.price;
    }

    if (payload.type === "decrease") {
      if (state.totalQuantity > 0 && selectedItem.quantity >= 0) {
        state.totalQuantity -= 1;
        state.totalPrice = state.totalPrice - selectedItem.price;
      }
    }
  }
};

export default mutations;
