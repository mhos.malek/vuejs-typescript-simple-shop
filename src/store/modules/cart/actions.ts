import MutationTypes from "./mutation-types";
import ActionTypes from "./action-types";

import { ActionsTypeInterface, StateType } from "./types";
import { getCardDetailsApi } from "@/services/api-requests/product";
import { ActionTree } from "vuex";

const actions: ActionTree<StateType, any> & ActionsTypeInterface = {
  async [ActionTypes.GET_CARD_DETAILS]({ commit }) {
    commit(MutationTypes.REQUESTS_CART_LIST);
    try {
      const response = await getCardDetailsApi();
      commit(MutationTypes.REQUESTS_CART_LIST_FULFILLED, response);
    } catch (error) {
      commit(MutationTypes.REQUESTS_CART_LIST_FAILD);
    }
  }
};

export default actions;
