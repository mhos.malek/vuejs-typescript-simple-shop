import { StateType } from "./types";

const state: StateType = {
  cart: {
    products: []
  },
  simliarProducts: [],
  totalQuantity: 0,
  totalPrice: 0,
  requestCardPending: false,
  allProducts: []
};

export default state;
