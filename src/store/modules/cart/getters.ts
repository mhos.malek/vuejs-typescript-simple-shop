import { GettersType } from "./types";

const getters: GettersType = {
  totalQuantityGetter: state => state.totalQuantity,
  totalPriceGetter: state => state.totalPrice,
  productsInCartGetter: state => state.cart.products,
  similarProductsGetter: state => state.simliarProducts.length,
  requestCardPendingGetter: state => state.requestCardPending
};

export default getters;
