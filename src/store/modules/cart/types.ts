import { Commit } from "vuex";
import MutationTypes from "./mutation-types";
import ActionTypes from "./action-types";

// ProductDetails Type
interface ProductDetailsGeneralType {
  id: number;
  title: string;
  categories: Array<CategoryType>;
  description: string;
  foundeDate: number;
  images: Array<string>;
  mainImage: string;
  price: string;
}

interface CategoryType {
  id: number;
  name: string;
}
interface ProductDeatilsType {
  quantity?: number;
  id: number;
}
interface CartItemType {
  products: Array<ProductDeatilsType>;
}

// init State Types:
interface StateType {
  simliarProducts: Array<any>;
  cart: CartItemType;
  totalQuantity: number;
  totalPrice: number;
  requestCardPending: boolean;
  allProducts: Array<any>;
}

// default VUEX Types
interface ActionTypeDefault {
  commit: Commit;
}

// action Types
interface ActionsTypeInterface {
  [ActionTypes.GET_CARD_DETAILS]({ commit }: ActionTypeDefault): Promise<any>;
}

//payload Types

interface RemoveOrAddItemFromCartPayload {
  id: number;
}

interface ChangeQuantityOfProduct {
  id: number;
  type: "increase" | "decrease";
}

// mutations payload
type MutationsTypeInterface<S = StateType> = {
  [MutationTypes.REQUESTS_CART_LIST](state: S): void;
  [MutationTypes.REQUESTS_CART_LIST_FULFILLED](
    state: S,
    payload: ProductDeatilsType
  ): void;
  [MutationTypes.REQUESTS_CART_LIST_FAILD](state: S): void;
  [MutationTypes.REMOVE_ITEM_FROM_CART](
    state: S,
    payload: RemoveOrAddItemFromCartPayload
  ): void;
  [MutationTypes.ADD_NEW_ITEM_TO_CART](
    state: S,
    payload: RemoveOrAddItemFromCartPayload
  ): void;
  [MutationTypes.REMOVE_ALL_ITEMS_IN_CART](state: S): void;
  [MutationTypes.CHANGE_ITEM_IN_CART](
    state: S,
    payload: ChangeQuantityOfProduct
  ): void;
};

// getters
type GettersType = {
  totalQuantityGetter(state: StateType): number;
  productsInCartGetter(state: StateType): Array<ProductDeatilsType>;
  totalPriceGetter(state: StateType): number;
  similarProductsGetter(state: StateType): number;
  requestCardPendingGetter(state: StateType): boolean;
};

export {
  ActionTypeDefault,
  ActionsTypeInterface,
  ProductDetailsGeneralType,
  ProductDeatilsType,
  StateType,
  ChangeQuantityOfProduct,
  MutationsTypeInterface,
  GettersType
};
