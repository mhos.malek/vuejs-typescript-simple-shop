import { GettersType } from "./types";

const getters: GettersType = {
  selectedProductDeatilGetter: state => state.selectedProductDetails,
  hasErrorGetter: state => state.productFindError
};

export default getters;
