import { Commit } from "vuex";
import MutationTypes from "./mutation-types";
import ActionTypes from "./action-types";

// ProductDetailType
interface ProductDetailType {
  id: number;
  title: string;
  categories: Array<CategoryType>;
  description: string;
  foundeDate: number;
  images: Array<string>;
  mainImage: string;
  price: string;
}

interface CategoryType {
  id: number;
  name: string;
}

// init State Types:
interface StateType {
  requestProductDetailPending: boolean;
  selectedProductDetails: null | ProductDetailType;
  productFindError: boolean;
}

// default VUEX Types
interface ActionTypeDefault {
  commit: Commit;
}

// action Types
interface GetProductByIdActionPayloadType {
  productId: number;
}

// MutationTypes
type MutationsTypeInterface<S = StateType> = {
  [MutationTypes.REQUEST_PRODUCT_DETAILS](state: S): void;
  [MutationTypes.REQUESTS_PRODUCT_DETAIL_FULFILLED](
    state: S,
    payload: ProductDetailType
  ): void;
  [MutationTypes.REQUESTS_PRODUCT_DETAIL_FAILED](state: S): void;
};

// default VUEX Types
interface ActionTypeDefault {
  commit: Commit;
}

// actions
interface ActionsTypeInterface {
  [ActionTypes.GET_PRODUCT_BY_ID](
    { commit }: ActionTypeDefault,
    payload: any
  ): Promise<any>;
}

// getters
type GettersType = {
  selectedProductDeatilGetter(state: StateType): ProductDetailType | null;
  hasErrorGetter(state: StateType): boolean;
};

export {
  ActionsTypeInterface,
  MutationsTypeInterface,
  GetProductByIdActionPayloadType,
  ProductDetailType,
  StateType,
  GettersType
};
