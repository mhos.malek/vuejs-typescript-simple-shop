import { StateType } from "./types";

const initialState: StateType = {
  requestProductDetailPending: false,
  selectedProductDetails: null,
  productFindError: false
};

export default initialState;
