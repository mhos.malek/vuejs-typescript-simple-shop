enum MutationTypes {
  REQUEST_PRODUCT_DETAILS = "REQUEST_PRODUCT_DETAILS",
  REQUESTS_PRODUCT_DETAIL_FULFILLED = "REQUESTS_PRODUCT_DETAIL_FULFILLED",
  REQUESTS_PRODUCT_DETAIL_FAILED = "REQUESTS_PRODUCT_DETAIL_FAILED"
}

export default MutationTypes;
