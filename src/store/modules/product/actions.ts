import MutationTypes from "./mutation-types";
import ActionTypes from "./action-types";

import { getProductByIdApi } from "@/services/api-requests/product";
import { ProductDetailType, ActionsTypeInterface } from "./types";
import { ActionTree } from "vuex";
import { StateType } from "../cart/types";

const actions: ActionTree<StateType, any> & ActionsTypeInterface = {
  async [ActionTypes.GET_PRODUCT_BY_ID]({ commit }, payload): Promise<void> {
    const { productId } = payload;
    commit(MutationTypes.REQUEST_PRODUCT_DETAILS);
    try {
      const response: ProductDetailType | unknown = await getProductByIdApi(
        productId
      );
      commit(MutationTypes.REQUESTS_PRODUCT_DETAIL_FULFILLED, response);
    } catch (error) {
      commit(MutationTypes.REQUESTS_PRODUCT_DETAIL_FAILED);
    }
  }
};

export default actions;
