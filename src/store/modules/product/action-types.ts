enum ActionTypes {
  GET_PRODUCT_BY_ID = "GET_PRODUCT_BY_ID"
}

export default ActionTypes;
