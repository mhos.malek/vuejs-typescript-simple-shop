import MutationTypes from "./mutation-types";
import { StateType, MutationsTypeInterface } from "./types";
import { MutationTree } from "vuex";

const mutations: MutationTree<StateType> & MutationsTypeInterface = {
  [MutationTypes.REQUEST_PRODUCT_DETAILS](state) {
    state.requestProductDetailPending = true;
    state.productFindError = false;
  },

  [MutationTypes.REQUESTS_PRODUCT_DETAIL_FULFILLED](state, payload) {
    state.requestProductDetailPending = false;
    state.selectedProductDetails = payload;
    state.productFindError = false;
  },
  [MutationTypes.REQUESTS_PRODUCT_DETAIL_FAILED](state) {
    state.requestProductDetailPending = false;
    state.productFindError = true;
  }
};

export default mutations;
