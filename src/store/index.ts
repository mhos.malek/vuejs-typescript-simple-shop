import Vue from "vue";
import Vuex from "vuex";
import { createLogger } from "vuex";

Vue.use(Vuex);

import cart from "./modules/cart";
import products from "./modules/product";
import auth from "./modules/auth";

export default new Vuex.Store({
  plugins: [createLogger()],
  modules: {
    cart,
    products,
    auth
  }
});
