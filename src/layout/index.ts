import TheMainLayout from "./TheMainLayout/TheMainLayout.vue";
import TheHeader from "./TheHeader/TheHeader.vue";
import TheFooter from "./TheFooter/TheFooter.vue";
import TheActionBar from "./TheActionBar/TheActionBar.vue";

export { TheMainLayout, TheHeader, TheFooter, TheActionBar };
