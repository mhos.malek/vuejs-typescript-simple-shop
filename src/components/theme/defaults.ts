type ThemeType = {
  pallete: {
    [x: string]: string;
  };
  typography: {
    [x: string]: string;
  };
  dimensions: {
    [x: string]: string;
  };
};
const ThemeDefaults: ThemeType = {
  pallete: {
    primary: "#06ae8e",
    secondary: "#5bc1df",
    gray: "#4a4a4a",
    danger: "#d13d3c"
  },
  typography: {
    defaultFontSize: "16px",
    defaultFont: "Open Sans"
  },
  dimensions: {
    default: "2rem"
  }
};

export default ThemeDefaults;
