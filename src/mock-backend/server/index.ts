import delay from "@/utils/genral";
import StorageServices from "@/services/storage";
import ERROR_MESSAGES from "@/error-handler/messages";

import productDetailsData from "@/mock-backend/data/product-details.json";
import cartData from "@/mock-backend/data/cart.json";
import { ProductDeatilsType } from "@/store/modules/cart/types";

class Backend {
  SERVER_DELAY_TIME: number;
  constructor() {
    this.SERVER_DELAY_TIME = 1500;
  }
  getProductById(
    showError = false,
    id: number
  ): Promise<string | ProductDeatilsType> {
    const data = productDetailsData.products.filter(
      product => product.id == id
    )[0];
    return new Promise((resolve, reject) => {
      if (showError || !productDetailsData || !data) {
        reject(ERROR_MESSAGES.NETWORK_DEFAULT_MESSAGE);
      }
      // simulate server response
      return delay(this.SERVER_DELAY_TIME).then(() => resolve(data));
    });
  }

  getCartDetails(showError = false) {
    return new Promise((resolve, reject) => {
      if (showError || !productDetailsData) {
        reject(ERROR_MESSAGES.NETWORK_DEFAULT_MESSAGE);
      }
      // simulate server response delay
      return delay(this.SERVER_DELAY_TIME).then(() => resolve(cartData));
    });
  }

  getCardRelatedProducts(showError = false) {
    return new Promise((resolve, reject) => {
      if (showError || !productDetailsData) {
        reject(ERROR_MESSAGES.NETWORK_DEFAULT_MESSAGE);
      }
      // simulate server response delay
      return delay(this.SERVER_DELAY_TIME).then(() => resolve(cartData));
    });
  }

  login(showError = false) {
    return new Promise((resolve, reject) => {
      if (showError) {
        reject(ERROR_MESSAGES.NETWORK_DEFAULT_MESSAGE);
      }
      StorageServices.setItem("dummyAccessToken", Math.random());
      resolve();
    });
  }
  logout(showError = false) {
    return new Promise((resolve, reject) => {
      if (showError) {
        return reject("Error in fetch Data");
      }
      StorageServices.removeItem("dummyAccessToken");
      resolve();
    });
  }
}

export default new Backend();
