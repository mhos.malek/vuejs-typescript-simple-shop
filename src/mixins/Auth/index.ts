import Vue from "vue";
import { createNamespacedHelpers } from "vuex";
import ActionTypes from "@/store/modules/auth/action-types";
import MutationTypes from "@/store/modules/auth/mutation-types";

const { mapMutations, mapActions, mapGetters } = createNamespacedHelpers(
  "auth"
);

// services
import StorageService from "@/services/storage/index";

export default Vue.extend({
  created: function() {
    this.checkUserAuthentication();
    if (!this.isAuthenticatedFromAuthMixin) {
      // handle user is not authenticatd
      // for this code challeng I havent authenticated user so we dont have login page and I redirect him/her to main page
    }
    const hasToken = StorageService.getItem("dummyAccessToken");
    if (hasToken) {
      // login user in store
      this[MutationTypes.REQUESTS_USER_LOGIN_FULFILLED]();
    } else {
      // logout user in store
      this[MutationTypes.REQUESTS_USER_LOGOUT_FULFILLED]();
    }
  },
  computed: {
    ...mapGetters({
      isAuthenticatedFromAuthMixin: "getAuthenticationStatusGetter",
      isAuthenticationPending: "getAuthenticationIsPendingGetter"
    })
  },
  methods: {
    ...mapActions({
      loginUserFromAuthMixin: ActionTypes.LOGIN_USER_ACTION,
      logOutUserFromAuthMixin: ActionTypes.LOGOUT_USER_ACTION
    }),
    ...mapMutations([
      MutationTypes.REQUESTS_USER_LOGIN_FULFILLED,
      MutationTypes.REQUESTS_USER_LOGOUT_FULFILLED
    ]),
    checkUserAuthentication: function() {
      if (!this.isAuthenticationPending && this.isAuthenticatedFromAuthMixin) {
        return true;
      }
      return false;
    }
  }
});
