export enum CurrenciesType {
  "Dollar" = "$",
  "Pound" = "£",
  "Euro" = "€"
}
export default function(
  value: string | number,
  currency: CurrenciesType = CurrenciesType.Dollar
) {
  if (!value) return "";
  value = value.toString();
  return `${value} ${currency}`;
}
