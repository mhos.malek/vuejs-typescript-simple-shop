export default function(value: string | number) {
  if (!value) return "";
  return new Date(value).toLocaleDateString("en-US").slice(-4);
}
